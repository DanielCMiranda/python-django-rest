# Installation guide

""" Requirements

* [Brew](https://brew.sh/)


""" Install required tools

Execute the following commands to install the required tools:

`brew install pyenv`

`pyenv install 3.7.4`

`pip install pipenv --user`


""" Install project dependencies

**On the project root folder**, execute the following command:

`pipenv install --dev --python ~/.pyenv/versions/3.7.4/bin/python`


""" Run the project locally

`make run`


""" Validate it is working

* Add author

```
curl -X POST -H "Content-Type: application/json" \
    http://127.0.0.1:8000/api/author/ \
    -d '{"name": "Fulano", "email": "fulano@bla.com"}'
```

* List authors

```
curl -X GET -H "Content-Type: application/json" http://127.0.0.1:8000/api/author/
```

* Add post

```
curl -X POST -H "Content-Type: application/json" \
    http://127.0.0.1:8000/api/post/ \
    -d '{"title": "My first post", "content": "This is what we have for today", "author": 1}'
```

* List posts

```
curl -X GET -H "Content-Type: application/json" http://127.0.0.1:8000/api/post/
```

* Add comment

```
curl -X POST -H "Content-Type: application/json" \
    http://127.0.0.1:8000/api/comment/ \
    -d '{"name": "John", "email": "mymail@mailer.com", "content": "This is it", "post": 1}'
```

* List comments

```
curl -X GET -H "Content-Type: application/json" http://127.0.0.1:8000/api/comment/
```

* Add post with author

```
curl -X POST -H "Content-Type: application/json" \
    http://127.0.0.1:8000/api/post-author/ \
    -d '{"title": "My other post", "content": "Lets do it", "author": {"name": "Ana", "email": "ana@bla.com"}}'
```
