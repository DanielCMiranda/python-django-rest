from rest_framework import status
from rest_framework.generics import ListCreateAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Author, Comment, Post
from .serializers import (AuthorSerializer, CommentSerializer,
                          PostAuthorSerializer, PostSerializer)


class AuthorListCreate(ListCreateAPIView):
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer


class PostListCreate(ListCreateAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer


class CommentListCreate(ListCreateAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer


class PostAuthorNestedView(APIView):
    def post(self, request):
        """
        Create a post record together with the specified author
        :param format: Format of the post records to return to
        :param request: Request object for creating the post
        :return: Returns a post record
        """
        serializer = PostAuthorSerializer(data=request.data)

        if serializer.is_valid(raise_exception=ValueError):
            serializer.create(validated_data=request.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.error_messages, status=status.HTTP_400_BAD_REQUEST)
