from django.db import models


class Author(models.Model):
    name = models.CharField(max_length=255)
    email = models.EmailField(unique=True, max_length=75)
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '%s: %s' % (self.id, self.name)


class Post(models.Model):
    title = models.CharField(unique=True, max_length=255)
    content = models.TextField()
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '%s: %s' % (self.id, self.title)


class Comment(models.Model):
    name = models.CharField(max_length=42)
    email = models.EmailField(max_length=75)
    content = models.TextField()
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '%s: %s' % (self.id, self.name)
