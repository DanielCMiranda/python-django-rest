from django.urls import path
from . import views


urlpatterns = [
    path("api/author/", views.AuthorListCreate.as_view()),
    path("api/post/", views.PostListCreate.as_view()),
    path("api/comment/", views.CommentListCreate.as_view()),
    path("api/post-author/", views.PostAuthorNestedView.as_view()),
]
