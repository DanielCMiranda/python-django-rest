from rest_framework.serializers import ModelSerializer
from .models import Author, Post, Comment


class AuthorSerializer(ModelSerializer):
    class Meta:
        model = Author
        fields = ("id", "name", "email", "created_on")


class PostSerializer(ModelSerializer):
    class Meta:
        model = Post
        fields = ("id", "title", "content", "author", "created_on")


class CommentSerializer(ModelSerializer):
    class Meta:
        model = Comment
        fields = ("id", "name", "email", "content", "post", "created_on")


class PostAuthorSerializer(ModelSerializer):
    author = AuthorSerializer(required=True)

    class Meta:
        model = Post
        fields = ("id", "title", "content", "author", "created_on")

    def create(self, validated_data):
        """
        Overriding the default create method of the Model serializer.
        :param validated_data: data containing all the details of the post
        :return: returns a successfully created post record
        """
        author_data = validated_data.pop('author')
        author = AuthorSerializer.create(AuthorSerializer(), validated_data=author_data)
        post, _ = Post.objects.update_or_create(
            author=author,
            defaults=validated_data,
        )
        return post
